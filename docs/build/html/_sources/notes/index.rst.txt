Release Log
===========

4 Mar 2019: Toggletator 1.3
--------------------------------

* New scripts: ``Toggletator Settings``, ``Toggle Layers Visibility by Labels``, ``Toggle Brush Blend Mode``,  ``Toggle Inactive Layers Visibility``, ``Toggle Brush Color Dynamics``, ``Toggle Brush Tilt``;
* Installer updated to support CC2019;

24 May 2018: Toggletator 1.2.1
--------------------------------

* Fixed ``Flip`` script not working properly on some machines;

24 Feb 2018: Toggletator 1.2
--------------------------------

+ New ``Toggle Flip`` script
+ Performance improvement

* Fixed ``Toggle Channel Visibility`` script not working properly after the last update

26 Jan 2018: Toggletator 1.1
------------------------------

+ Fixed scripts not installing properly to CC2018 on Mac
+ new ``Ignore Color Labels`` option for ``toggle_layer_visibility.jsx``
+ new ``Toggle Zoom`` script

17 Oct 2017: Toggletator 1.0
------------------------------------


.. |br| raw:: html

   <br />