Release Log
===========

7 July 2024: Toggletator 1.6.1
--------------------------------

* a new script: ``Toggle Selection``;
* a new script: ``Toggle Colorpicker Sample``;

--------------------------------

27 Jan 2024: Toggletator 1.6.0
--------------------------------

* A much faster Panel version of the panel;
* Fixed issues that came with the release of Photoshop 25.4;

--------------------------------

5 Dec 2022: Toggletator 1.5.0
--------------------------------

* New scripts: ``Toggle Opacity Control``, ``Toggle Sample All setting``, ``Cycle Channels``, ``Toggle between 2 presets``;
* Toggle Blendmode includes an option to always switch to the first blendig mode when switching from a different tool;
* Toggle Dynamics Apply per Tip option added;
* Support for new Photoshop version folders;


19 Nov 2019: Toggletator 1.4.2
--------------------------------
* Installers updated to support Photoshop 2020;
* Installers will remove readonly flag from Scripts folder of Photoshop;
* Fixed: unability to set hotkeys to specific keys using the Settings panel;

24 Sep 2019: Toggletator 1.4.1
--------------------------------

* ``Flip horizontal`` has an option to preserve a selection (`On` by default);
* ``Flip horizontal`` has an option to flip a tip of the current brush horizontally;
* Fixed some issues with ``Flip horizontal`` in ``Horizontal Flip View`` mode;

3 Sep 2019: Toggletator 1.4.0
--------------------------------

* it's possible to assign script hotkeys directly from ``Toggletator Settings`` script;
* ``Flip Horizontal`` now allows to use a new CC2019 Flip Horizontal View command which should work faster than flipping actual canvas pixels;
* ``Flip horizontal`` also includes new Shift X and Shift Y fields in the options  if you want to shift a visual center against which the flip is made — for example if you use fullscreen mode or generally work in a particular part of the screen;
* also ``Flip Horizontal``: there's an option to disable my makeshift way of resetting canvas rotation — before the script was selecting a Crop Tool to reset it but if you don't really rotate your canvas that's superfluous. So the option is off by default;

4 Mar 2019: Toggletator 1.3.0
--------------------------------

* New scripts: ``Toggletator Settings``, ``Toggle Layers Visibility by Labels``, ``Toggle Brush Blend Mode``,  ``Toggle Inactive Layers Visibility``, ``Toggle Brush Color Dynamics``, ``Toggle Brush Tilt``;
* Installer updated to support CC2019;

24 May 2018: Toggletator 1.2.1
--------------------------------

* Fixed ``Flip`` script not working properly on some machines;

24 Feb 2018: Toggletator 1.2
--------------------------------

+ New ``Toggle Flip`` script
+ Performance improvement

* Fixed ``Toggle Channel Visibility`` script not working properly after the last update

26 Jan 2018: Toggletator 1.1
------------------------------

+ Fixed scripts not installing properly to CC2018 on Mac
+ new ``Ignore Color Labels`` option for ``toggle_layer_visibility.jsx``
+ new ``Toggle Zoom`` script

17 Oct 2017: Toggletator 1.0
------------------------------------


.. |br| raw:: html

   <br />