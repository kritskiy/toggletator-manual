Toggletator Manual
====================

.. image:: img/01_2.gif

Toggletator is a collection of scripts for Photoshop that toggle different things: visibility of specific layers, document zoom, brush blending modes, etc with a hotkey. It contains:

* :ref:`Toggle Layers <tlayer>` (by name or color label);
* :ref:`Toggle Active Layers <tactive>`;
* :ref:`Toggle Inactive Layers <tinactive>`;
* :ref:`Toggle Channels <tchannel>`;
* :ref:`Toggle Zoom <tzoom>`;
* :ref:`Toggle Flip <tflip>`;
* :ref:`Toggle Brush Blend Mode <tblend>`;
* :ref:`Toggle Brush Color Dynamics <tdynamics>`;
* :ref:`Toggle Brush Tilt Control <ttilt>`;
* :ref:`Toggle Brush Size Control <tgsize>`;
* :ref:`Toggle Brush Opacity Control <tgtransfer>`;
* :ref:`Toggle Sample All <tgsample>`;
* :ref:`Toggle Between 2 Presets <tg2presets>`;
* :ref:`Toggle Selection <tgselection>`;
* :ref:`Toggle between 2 Colopicker Samples <tgpicker>`;
* :ref:`Cycle Channels Visibility <cyclechannels>`;


Contact me at kritskiy.sergey@gmail.com |br|\
Twitter: `@ebanchiki <https://twitter.com/ebanchiki>`_ |br|\
Grab it on `Cubebrush <https://cubebrush.co/kritskiy>`_ or `Gumroad <https://gumroad.com/kritskiy>`_ 

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   install/index
   settings/index
   scripts/index
   notes/index
   
.. |br| raw:: html

   <br />