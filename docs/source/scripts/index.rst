Scripts
==========================

.. _tlayer:

Toggle Layer Visibility (by name)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles visibility of layers with a specific prefix

.. image:: img/02_2.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Defaul prefixes are: ``hide``, ``cont`` and prefixes of `Perpsective Tools <https://gumroad.com/l/MESl>`_: ``perspective`` and ``parallel``;

Advanced:

By default the script will ignore layers with color labels (I use this to have different perspective grids and sketch layers in a document), this option can be turned off in settings: use the :ref:`Toggletator Settings <settings>` script to change it and modify prefixes;

--------------

Toggle Layer Visibility (by color label)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles visibility of layers with a specific color label

.. image:: img/labels.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Default color label is Red;

Advanced:

Use the :ref:`Toggletator Settings <settings>` script to change the color label to toggle;

--------------

.. _tactive:

Toggle Active Layers
~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles visibility of selected layers.
	* This is useful to switch between two versions of a layer/groups of layers;
	* To check how layers or selected adjustment layers are affecting the lower layers;

.. image:: img/03_2.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;

--------------

.. _tinactive:

Toggle Inactive Layers
~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles visibility of layers that aren't selected (that are currently visible).
	* This is useful to check specific layers without context;
	* To find hidden layers in the document;

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;

--------------

.. _tchannel:

Toggle Channel Visibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles visibility of channels with specific prefix.

.. image:: img/04_2.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Defaul prefixes are: ``hide`` and ``cont``;

Advanced:

* Having a channel is useful if working with several windows of one document:
* Channels may be visible only in one window;
* Eyedropper or Mixer Brush won't pick occasional line art or grid colors from channels;
* To change the prefixes, modify the settings via the :ref:`Toggletator Settings <settings>`.

--------------------------------

.. _cyclechannels:

Cycle Between Channels Visibility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* Similar to Toggle Channel Visibility, this script will cycle though visibility of specific channels;

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Defaul prefixes are: ``hide`` and ``cont``;

--------------

.. _tzoom:

Toggle Zoom
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles between two zoom levels;
* It only works in Photoshop versions from CC2015;

.. image:: img/05_2.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Change options with the :ref:`Toggletator Settings <settings>`;

Advanced:

* with a ``Smart Zoom`` option turned ON Toggle Zoom will toggle between two last used zoom extremes. For example, you were at 25% zoom, calling the function will zoom to 300%. Next time you call the function, you'll unzoom back to 25%. If you unzoom to 16.67%, first call bring you back to 300%, second — to 16.67%.
* with a ``Smart Zoom`` option set to OFF Toggle Zoom will toggle zoom between current zoom value and value from ``Toggle to zoom`` option (default is 25%). Yes, I used a word zoom 5 times in this sentence.

--------------

.. _tflip:

Toggle Flip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script flips view or canvas horizontally (by default) or canvas vertically while maintaining visual center of the document;
* It only works in Photoshop versions from CC2015;

.. image:: img/flip.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Change options with the :ref:`Toggletator Settings <settings>`;

Advanced:

* Set ``Type of flip`` option to set flipping canvas horizontally/vertically or flip view horizontally (CC2019+);
* if you're using a HDPI display (4K, 5K, Retina display on Mac), change ``Display Scale Factor`` option to the value of your zoom factor. For Mac Retina it's ``200``. For Windows get a percent value from Display Settigns;
* By default the visual center of the document is located in the center of the active window but it's possible to shift it using ``Offset Center`` values: for instance if you work on a particular side of a screen in Fullscreen mode;
* Since it's impossible to set rotation of a document using a script, document rotation will be reseted.

--------------

.. _tblend:

Toggle Brush Blend Mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles Brush Tool between two defined blend modes;
* Basic usecase would be ``Normal`` and ``Erase`` (defaul settings);
* If current tool isn't a Brush Tool, Brush Tool will be selected;
* It only works in Photoshop versions from CC2014;

.. image:: img/blend.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Change options with the :ref:`Toggletator Settings <settings>`;

--------------------------------

.. _tdynamics:

Toggle Brush Color Dynamics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles Color Dynamics with particular settings on and off;
* If current tool isn't a Brush Tool, Brush Tool will be selected;
* It only works in Photoshop versions from CC2014;

.. image:: img/dyn.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Change options with the :ref:`Toggletator Settings <settings>`;

--------------------------------

.. _ttilt:

Toggle Brush Tilt Control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles Angle Control in Shape Dynamics setting of a brush between two defined options;
* The default setting toggles between current brush Angle and Pen Tilt; 
* If current tool isn't a Brush Tool, Brush Tool will be selected;
* It only works in Photoshop versions from CC2015;

.. image:: img/tilt.gif

Usage:

* :ref:`Assign script to a hotkey <hotkey>`;
* Change options with the :ref:`Toggletator Settings <settings>`;

--------------------------------

.. _tgsize:

Toggle Brush Size Control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles Size Control in Shape Dynamics setting of a brush between two defined options;
* The default setting toggles between Pen Pressure and Off; 
* If current tool isn't a Brush Tool, Brush Tool will be selected;
* It only works in Photoshop versions from CC2015;

--------------------------------

.. _tgtransfer:

Toggle Brush Opacity Control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles Opacity Control in Transfer setting of a brush between two defined options;
* The default setting toggles between Pen Pressure and Off; 
* There's also an option to disable Flow Control when the setting is set to Off to make sure nothing adds to the transparency of the preset;
* If current tool isn't a Brush Tool, Brush Tool will be selected;
* It only works in Photoshop versions from CC2015;

--------------------------------

.. _tgsample:

Toggle Sample All Setting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles Sample All setting for tools that support it (Mixer Brush, Healing Brush, etc);
* It only works in Photoshop versions from CC2015;

--------------------------------

.. _tg2presets:

Toggle Between 2 Presets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles between 2 predefined brush or tool presets;
* It only works in Photoshop versions from CC2015;
* Set the preset names and types in the settings;

Note that Photoshop doesn't require unique names for brush presets so it's possible to have several presets with the same name. In this case a wrong preset could be selected. If you're getting a non-expected brush selected, try renaming the desired preset to something unique-looking;

---------------------------------

.. _tgselection:

Toggle Selection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles between the last used selection and no selection;
* It only works in Photoshop versions from CC2015;
* This script doesn't have any options;

---------------------------------

.. _tgpicker:

Toggle Colopicker Sample
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Overview:

* This script toggles between 2 predefined Eyepicker tool sample modes;
* It only works in Photoshop versions from CC2015;
* Set the sample modes to use in the settings;


.. |br| raw:: html

   <br />