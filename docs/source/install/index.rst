Installation
==========================

#. Close Photoshop
#. To install: 
	* Run ``.exe`` file from Windows Install or ``.pkg`` file from Mac Install for quick install;
	* Or copy ``*.jsx`` files from Manual Install to ``Scripts`` folder of your Photoshop;

-----------------------

.. _hotkey:

Assigning to Hotkey
~~~~~~~~~~~~~~~~~~~~

To assign scripts to shortcuts:

#. Start Photoshop and navigate to ``Edit > Keyboard Shortcuts`` menu;
#. In the Keyboard Shortcuts menu select ``Keyboard Shortcuts``;
#. In ``Shortcuts For`` field select ``Application Menus``;
#. In ``Application Menu Command`` find all the scripts in ``File > Scripts`` and assign desired shortcuts

.. image:: img/hotkeys.png

.. |br| raw:: html

   <br />