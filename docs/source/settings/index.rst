.. _settings:

Toglettator Settings
===================================

``Toglettator Settings`` is a special script used specifically for changing settings and managing scripts.

.. image:: img/settings.gif

You can access it in several ways:

* through ``File > Scripts > ! Toggletator Settings`` Photoshop menu item;
* through ``Edit > Search`` Photoshop menu item, typing ``!`` and selecting ``! Toggletator Settings``;
* if you have `Brusherator <http://gum.co/brusherator>`_ installed, flyout menu includes ``Toggletator Settings`` item;

--------------------------------

Managing scripts
--------------------------------

The first row of buttons:

* Select a script you want to modify from the list;
* ``Send to Brusherator``: if `Brusherator <http://gum.co/brusherator>`_ is installed, a current script will be added to an active shelf of any Brusherator;

The second row allows you to manage script file:

* ``File Info...`` will show a window with options to copy file path to clipboard and open Scripts folder in file explorer;
* ``Duplicate`` will make a copy of a current script with a new name. Use this to create several instances of scripts with different settings;
* ``Rename`` will rename a current script;
* ``Delete`` will remove a current script;

Last 3 options may be greyed out if Photoshop doesn't have access to its ``Presets/Scripts`` folder. To give PS access:

* on Mac: 
	* make sure ``Applications/Adobe Photoshop CC XXXX/Presets/Scripts`` folder isn't set to ``Read Only``;
* on Windows:
	* retake ownership of the folder (https://superuser.com/a/1141616/971910);
	* OR simply restart PS as Administrator:

.. image:: img/as_admin.png

--------------------------------


.. |purp| raw:: html

   <span class="purp">📝 Description: </span>

.. |analog| raw:: html

   <span class="purp">💼 Photoshop analogue: </span>

.. |why| raw:: html

   <span class="purp">👍 Usage: </span>

.. |br| raw:: html

	<br />

.. |tr| raw:: html

	<br /><span class="item">* </span>